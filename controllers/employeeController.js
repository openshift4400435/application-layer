const db = require("../models");
const Employee = db.employee;
const { Op } = require("sequelize");

const add = async (req, res) => {
  try {
    let info = {
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email,
      education: req.body.education,
      position: req.body.position
    };

    let newEmployee = await Employee.create(info);

    res
      .status(200)
      .send({ newEmployee, message: "Employee added" });
  } catch (error) {
    res.status(500).send(error);
    console.log(error);
  }
};

const getAllEmployee = async (req, res) => {
  try {
    let allEmployee = await Employee.findAll();

    res.status(200).send(allEmployee);
  } catch (error) {
    console.log(error);
    res.status(500).send({ error, message: "Internal server error" });
  }
};


module.exports = {
  add,
  getAllEmployee
};