const employeeController = require("../controllers/employeeController");
const router = require("express").Router();

router.post("/add", employeeController.add);
router.get("/all", employeeController.getAllEmployee);

module.exports = router;