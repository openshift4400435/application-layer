require("dotenv").config();
const express = require("express");
const cors = require("cors");
const PORT = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => {
  res.status(200).json({
    message:
      "Employee CRUD API.",
  });
});

// routers
const employeeRouter = require("./routers/employeeRouter");

// main route
app.use("/employee", employeeRouter);

app.listen(PORT, () => console.log(`server running: ${PORT}`));