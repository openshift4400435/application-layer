const { DataTypes, Sequelize } = require("sequelize");

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: "mysql",
    port: process.env.DB_PORT,
    timezone: "+07:00",
    // pool: {
    //   max: 5,
    //   min: 0,
    //   acquire: 30000,
    //   idle: 10000,
    // },
  }
);

// testing connection
sequelize
  .authenticate()
  .then(() => console.log(`CONNECTION SUCCESSFULL . . .`))
  .catch((err) => console.log(`ERROR: ${err}`));

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//all models
db.employee = require("./employeeModel")(sequelize, DataTypes);

// sync all model at once
db.sequelize.sync({ alter: true }).then(() => {
  console.log(`re-sync done!`);
});

module.exports = db;